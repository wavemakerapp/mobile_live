/*Copyright (c) 2015-2016 gmail.com All Rights Reserved.
 This software is the confidential and proprietary information of gmail.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with gmail.com*/
package com.mobile_widgets_variables.qrdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.mobile_widgets_variables.qrdb.Codescan;


/**
 * ServiceImpl object for domain model class Codescan.
 *
 * @see Codescan
 */
@Service("qrdb.CodescanService")
@Validated
public class CodescanServiceImpl implements CodescanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CodescanServiceImpl.class);


    @Autowired
    @Qualifier("qrdb.CodescanDao")
    private WMGenericDao<Codescan, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Codescan, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "qrdbTransactionManager")
    @Override
    public Codescan create(Codescan codescan) {
        LOGGER.debug("Creating a new Codescan with information: {}", codescan);

        Codescan codescanCreated = this.wmGenericDao.create(codescan);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(codescanCreated);
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager")
    @Override
    public Codescan getById(Integer codescanId) {
        LOGGER.debug("Finding Codescan by id: {}", codescanId);
        return this.wmGenericDao.findById(codescanId);
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager")
    @Override
    public Codescan findById(Integer codescanId) {
        LOGGER.debug("Finding Codescan by id: {}", codescanId);
        try {
            return this.wmGenericDao.findById(codescanId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No Codescan found with id: {}", codescanId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager")
    @Override
    public List<Codescan> findByMultipleIds(List<Integer> codescanIds, boolean orderedReturn) {
        LOGGER.debug("Finding Codescans by ids: {}", codescanIds);

        return this.wmGenericDao.findByMultipleIds(codescanIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "qrdbTransactionManager")
    @Override
    public Codescan update(Codescan codescan) {
        LOGGER.debug("Updating Codescan with information: {}", codescan);

        this.wmGenericDao.update(codescan);
        this.wmGenericDao.refresh(codescan);

        return codescan;
    }

    @Transactional(value = "qrdbTransactionManager")
    @Override
    public Codescan delete(Integer codescanId) {
        LOGGER.debug("Deleting Codescan with id: {}", codescanId);
        Codescan deleted = this.wmGenericDao.findById(codescanId);
        if (deleted == null) {
            LOGGER.debug("No Codescan found with id: {}", codescanId);
            throw new EntityNotFoundException(String.valueOf(codescanId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "qrdbTransactionManager")
    @Override
    public void delete(Codescan codescan) {
        LOGGER.debug("Deleting Codescan with {}", codescan);
        this.wmGenericDao.delete(codescan);
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager")
    @Override
    public Page<Codescan> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Codescans");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager")
    @Override
    public Page<Codescan> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Codescans");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service qrdb for table Codescan to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service qrdb for table Codescan to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "qrdbTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}