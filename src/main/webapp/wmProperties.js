var _WM_APP_PROPERTIES = {
  "activeTheme" : "blue-enterprise",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "Mobile_Widgets_Variables",
  "homePage" : "Main",
  "name" : "Mobile_Widgets_Variables",
  "platformType" : "MOBILE",
  "securityEnabled" : "false",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};